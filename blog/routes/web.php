<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/profile', 'ProfileController@profile');
Route::get('/post', 'PostController@post');
Route::get('/category', 'CategoryController@catIndex');
Route::post('comment/{id}', 'PostController@comment');
Route::get('/photo', 'PhotoController@photo');
Route::get('/news', 'NewsController@news');
Route::get('/video', 'VideoController@video');
Route::get('/community', 'CommunityController@community');
Route::get('/message', 'MessageController@message');
Route::get('/friend', 'FriendController@friend');
Route::get('/event', 'EventController@event');

Route::get('/viewPost/{id}', 'PostController@viewPost');
Route::get('/edit/{id}', 'PostController@edit');


Route::post('/addCategory','CategoryController@addCategory');
Route::post('/addProfile','ProfileController@addProfile');
Route::post('/addPost','PostController@addPost');
Route::post('/editPost/{id}','PostController@editPost');
Route::get('/delete/{id}','PostController@delete');

Route::get('category/{id}','CategoryController@category');

Route::get('like/{id}','PostController@like');

Route::get('dislike/{id}','PostController@dislike');


