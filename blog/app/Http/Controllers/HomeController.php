<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Profile;
use App\Post;
use App\user;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
 
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        // $results = Project::orderBy('created_at', 'desc')->get();
        // $posts = Post::all();
        $posts = Post::orderBy('created_at','desc')->get();
        $post_id = Post::all();
        $user_id = Auth::user()->id;
        $profile = DB::table('users')
            ->join('profiles','users.id','=','profiles.user_id')
            ->select('users.*','profiles.*')
            ->where(['profiles.user_id'=>      $user_id])
            ->first();
          //pass info to the home pass as an array
        //display comments 
        $comments = DB::table('users')
            ->join('comments', 'users.id', '=', 'comments.user_id')
            ->join('posts', 'comments.post_id', '=', 'posts.id')
            ->select('users.name', 'comments.*')
            ->where(['posts.id'=>$post_id])
            ->get();

        
        $posts = Post::orderBy('created_at','desc')->get();
        $categories = Category::all(); // get all from category
        return view('/home',['profile'=>$profile,'posts'=>$posts,'categories' => $categories,'comments'=>$comments]);
    }

    // public function category($cat_id){
    //     $categories = Category::all();
    //     $posts = DB::table('posts')
    //         ->join('categories', 'category_id', '=', 'categories.id')
    //         ->select('posts.*', 'categories.*')
    //         ->where(['categories.id'=>$cat_id])
    //         ->get();
    //     return view('categories.category-posts',['categories'=>$categories,'posts'=>$posts]);
    // }
}
