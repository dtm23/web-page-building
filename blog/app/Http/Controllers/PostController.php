<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use App\Category;
use App\Post;
use App\Like;
use App\Comment;
use App\Dislike;
use Auth;


class PostController extends Controller
{
    public function post(){
        $like_count = new Like;
		$categories = Category::all(); // get all from category
        $posts = Post::all();
    	return view('posts.post',['categories' => $categories,'posts'=>$posts,'like_count'=> $like_count]);
    }

    public function addPost(Request $request){
    	  	//create a server validate
        $this->validate($request,[
            'post_title'=> 'required',
            'post_body'=>'required',
            'category_id'=>'required',
            'post_image'=>'required'
        ]);

        //return 'validated all input';
        //create an object for profile model
        $posts = new Post;
        $posts->user_id = Auth::user()->id;
        $posts->post_title = $request->input('post_title');
        
        $posts->post_body = $request->input('post_body');

        $posts->category_id = $request->input('category_id');
        $posts->post_image = $request->input('post_image');
        //let's put image on public folder called uploads
        if(Input::hasFile('post_image')){
            $file = Input::file('post_image');
$file->move(public_path().'/post_image/',$file->getClientOriginalName());
$url = URL::to("/").'/post_image/'. $file->getClientOriginalName();
$posts->post_image = $url;
        }
        $posts->save();

        return redirect('/home')->with('message','Post Added Successfully');
    }

    public function viewPost($post_id){
        
        $posts = Post::where('id','=',$post_id)->get();
        $likePost = Post::find($post_id);

        $like_count = Like::where(['post_id' => $likePost->post_id])->count();
       
        $dislike_count = Dislike::where(['post_id' => $likePost->post_id])->count();

        $categories =Category::all();

        //display comments 
        $comments = DB::table('users')
            ->join('comments', 'users.id', '=', 'comments.user_id')
            ->join('posts', 'comments.post_id', '=', 'posts.id')
            ->select('users.name', 'comments.*')
            ->where(['posts.id'=>$post_id])
            ->get();


        return view('posts.view',['posts'=>$posts,'categories'=>$categories,'like_count' => $like_count,'dislike_count'=>$dislike_count,'comments'=>$comments]);

    }

    public function edit($post_id){
          //get view item from given id and pass it to view page
        $categories =Category::all();
        $posts = Post::find($post_id);
        //get a category name insted of id
        $category = Category::find($posts->category_id);

        return view('posts.edit',['posts' => $posts,'categories' =>
            $categories,'category' =>$category]);


    }

    public function editPost(Request $request,$post_id){
        //create a server validate
        $this->validate($request,[
            'post_title'=> '',
            'post_body'=>'required',
            'category_id'=>'',
            'post_image'=>''
        ]);

        //return 'validated all input';
        //create an object for profile model
        $posts = new Post;
        $posts->user_id = Auth::user()->id;
        $posts->post_title = $request->input('post_title');
        $posts->post_body = $request->input('post_body');
        $posts->category_id = $request->input('category_id');
        $posts->post_image = $request->input('post_image');
        //let's put image on public folder called uploads
        if(Input::hasFile('post_image')){
            $file = Input::file('post_image');
$file->move(public_path().'/post_image/',$file->getClientOriginalName());
$url = URL::to("/").'/post_image/'. $file->getClientOriginalName();
        $posts->post_image = $url;
        }

        $data = array(
            'user_id' => $posts->user_id, 
            'post_title' => $posts->post_title, 
            'post_body' => $posts->post_body, 
            'category_id' => $posts->category_id, 
            'post_image' =>$posts->post_image 
        );

        Post::where('id', $post_id)->update($data); 
        $posts->update();

        return redirect('/home')->with('message','Post Updated Successfully');
    }

    public function delete($post_id){
        Post::where('id',$post_id)->delete();
        return redirect('/home')->with('message','Post has been deleted!');

    }

    public function like($post_id){
        $loggedin_user = Auth::user()->id; //
        $like_user = Like::where(['user_id'=>$loggedin_user,'post_id'=>$post_id]);
        if(empty($like_user->user_id)){
            $user_id = Auth::user()->id;
            $email = Auth::user()->email;
            $post_id = $post_id;
            $like = new Like;
            $like->user_id = $user_id;
            $like->email = $email;
            $like->post_id = $post_id;

            $like->save();
            return redirect('/viewPost/{$id}');
        }else{
            return redirect('/viewPost/{$id}');
        }
    }
    public function dislike($post_id){
        $loggedin_user = Auth::user()->id; //
        $dislike_user = Like::where(['user_id'=>$loggedin_user,'post_id'=>$post_id]);
        if(empty($dislike_user->user_id)){
            $user_id = Auth::user()->id;
            $email = Auth::user()->email;
            $post_id = $post_id;
            $dislike = new Dislike;
            $dislike->user_id = $user_id;
            $dislike->email = $email;
            $dislike->post_id = $post_id;

            $dislike->save();
            return redirect('/viewPost/{$id}',['dislike'=>$dislike])->with('message','success dislike');
        }else{
            return redirect('/viewPost/{$id}');
        }
    }

    public function comment(Request $request, $post_id){
        //server side validation
        $this->validate($request,[ 'comment'=> 'required']);

        //instance of comment
        $comment = new Comment;
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $post_id;
        $comment->comment = $request->input('comment');
        $comment->save();

        return redirect('/viewPost/{$id}')->with('message','Comments has been added Successfuly!');

    }
}
