<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Profile;
use App\user;
use Auth;
class ProfileController extends Controller
{
    public function profile(){
        $user_id = Auth::user()->id;
        $profiles = DB::table('users')
                    ->join('profiles','user_id','=','profiles.user_id')
                    ->select(['users.*','profiles.*'])
                    ->where(['profiles.user_id' => $user_id])->first();

    	return view('profiles.profile', [ 'profiles' => $profiles ] );
    }

    public function addProfile(Request $request){


    	$this->validate($request,[
    		'name' => 'required',
    		'middle_name' => ' ',
    		'last_name' => 'required',
    		'phone_number' => ' ',
    		'current_location' => ' ',
    		'profile_status' => ' ',
    		'profile_availability' => ' ',
    		'designation' => 'required',
    		'profile_pic' => 'required'
    	]);
    	
    	$profiles = new Profile();
    	//user id goes first whoe's is logged in
    	
    	$profiles->user_id = Auth::user()->id;
    	$profiles->name = $request->input('name');
    	$profiles->middle_name = $request->input('middle_name');
    	$profiles->last_name = $request->input('last_name');
		$profiles->phone_number = $request->input('phone_number');	
		$profiles->current_location = $request->input('current_location');
		$profiles->profile_status = $request->input('profile_status');
		$profiles->profile_availability = $request->input('profile_availability');
		$profiles->designation = $request->input('designation');
		
		//let's put image on public folder called uploads
        if(Input::hasFile('profile_pic')){
            $file = Input::file('profile_pic');
$file->move(public_path().'/uploads/',$file->getClientOriginalName());
$url = URL::to("/").'/uploads/'. $file->getClientOriginalName();
        
        }
        $profiles->profile_pic= $url;

        $profiles->save();

		return redirect('/profile')->with('message',' Scuccessfully update to Profiles!');
    }

}
