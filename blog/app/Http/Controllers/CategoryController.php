<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Like;
use App\Dislike;
use App\Category;

class CategoryController extends Controller


{   public function catIndex(){
  return view('categories.category');
}
    public function category($post_id){


        $categories = Category::all();
        $posts = DB::table('posts')
            ->join('categories', 'category_id', '=', 'categories.id')
            ->select('posts.*', 'categories.*')
            ->where(['categories.id'=>$post_id])
            ->get();

      
        $likePost = Post::find($post_id);

        $like_count = Like::where(['post_id' => $likePost->post_id])->count();
       
        $dislike_count = Dislike::where(['post_id' => $likePost->post_id])->count();


        //display comments 
        $comments = DB::table('users')
            ->join('comments', 'users.id', '=', 'comments.user_id')
            ->join('posts', 'comments.post_id', '=', 'posts.id')
            ->select('users.name', 'comments.*')
            ->where(['posts.id'=>$post_id])
            ->get();


        return view('categories.category-posts',['posts'=>$posts,'categories'=>$categories,'like_count' => $like_count,'dislike_count'=>$dislike_count,'comments'=>$comments]);
    }
    	
    
   	public function addCategory(Request $request){
   		//server side valide first
   		$this->validate($request,[
   			'category'=>'required'
   		]);

   		$category = new Category;
   		$category->category = $request->input('category');
   		$category->save(); //save in database;
   		//now re-direct to category page again
   		return redirect('/category')->with('message',$category->category. ' added Scuccessfully to Category!');
   	}
}
