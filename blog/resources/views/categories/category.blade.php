@extends('layouts.app')
@section('title')
    Cateogry
@endsection
@section('content')
<!-- This layout for, POSTS-CATEGORY-EDIT PROFILE, MESSAGE, COMMUNIIES, PHOTO VIDEO -->
<div class="row">
    <div class="col-12 ">
        <!-- Categories Main events goes here -->
    	<div class="panel panel-default">
    		@if(count($errors)>0)
    			@foreach($errors->all() as $error)
    				<div class="alert alert-danger">$error</div>
    			@endforeach
    		@endif
    		@if(session('message'))
    			<div class="alert alert-success">{{session('message')}}</div>
    		@endif
            <div class="panel-heading text-center">A D D &nbsp; &nbsp; C A T E G O R I E S</div>
            <hr>

            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ url('/addCategory') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">

                        <div class="col-md-6">
                            <input id="category" type="category" class="form-control" name="category" value="{{ old('category') }}" placeholder="Add Category " required>

                            @if ($errors->has('category'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Add Category
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection 