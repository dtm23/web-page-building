@extends('layouts.app')
@section('title')
    Photo
@endsection
@section('content')
<!-- This layout for, POSTS-CATEGORY-EDIT PROFILE, MESSAGE, COMMUNIIES, PHOTO VIDEO -->
<div class="row">
    <div class="col-12 main-layer">
    	<div class="row">
    		<div class="col-12" ">
            @if(count($categories)>0)
                @foreach($categories as $category)
                <a href="{{ url('category/'.$category->id ) }}" class="badge badge-info">{{$category->category}}</a>
                @endforeach
            @endif
    		</div>
    		<div class="col-12">
    			   @if(count($posts)>0)
		            @foreach($posts as $post)
		                <div class="post-layer">
		                        <div class="" style="background-color: ;">
		                            
		                                <h5 style="padding-left: 13px;font-family: arial; color: #1F618D;">Title:    {{$post->post_title}}</h5>
		                            
		                            <img src=" {{$post->post_image}} " style="width: 100%">
		                            <br>
		                            <div style="margin: auto; width: 95%; padding:3px;background-color:lightgray;border-radius: 10px;margin-top: 3px;margin-bottom: -23px">
		                                {{substr($post->post_body,0,200)}}
		                            </div>
		                            <br>
		                            <nav class="nav">
		                               <a class="nav-link" href="{{url('/viewPost/'.$post->id)}}">
		                                    <span class="fa fa-eye" > View</span></a>
		                               <a class="nav-link" href="{{ url('/edit/'.$post->id) }}">
		                                <span class="fa fa-pencil-square-o" style="padding-left: 0px;"> Edit</span></a>
		                               <a class="nav-link" href="{{ url('/delete/'.$post->id) }}"><span class="fa fa-trash" style="padding-left: 0px;"> Delete</span></a>
		                             
		                            </nav>
		                            <cite style="padding-left: 13px">
		                                Posted on: {{ date('M j, Y H:i', strtotime($post->updated_at))}}
		                            </cite>
		                        </div>
		                </div>
		            @endforeach
		        @else
		            <p>NO POSTS HAS BEEN UPDATED </p>
		        @endif
    			
    		</div>
              
    	</div>
    </div>
</div>
@endsection