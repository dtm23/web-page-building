@extends('layouts.app')
@section('title')
    Profile
@endsection
@section('content')
<!-- This layout for, POSTS-CATEGORY-EDIT PROFILE, MESSAGE, COMMUNIIES, PHOTO VIDEO -->
<div class="row">
    <div class="col-12">
        <!-- Categories Main events goes here -->
        <div class="panel panel-default">
            @if(count($errors)>0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">$error</div>
                @endforeach
            @endif
            @if(session('message'))
                <div class="alert alert-success">{{session('message')}}</div>
            @endif
            <div class="panel-heading text-center">EVENTS</div>
            <hr>

            <div class="panel-body">
               <div class="row">
                    <div class="col-4">
                        <!-- profile picture set up here! -->
                        <div class="">
                            @if(!empty($profiles))
                                <img src="{{ $profiles->profile_pic }}" class="avatar">
                            @else
                                <img src="{{ url('images/avatar.ico') }}" class="avatar">
                            @endif
                        </div>
                    </div>
                    <div class="col-8">
                        <!-- Users-info-goes here -->
                        @if(count($profiles)>0)
                            <div class="row">
                                <div class="col-9">
                                   Status: {{$profiles->profile_status}}            
                               </div>
                                <div class="col-3">
                                    <p class="text-right">{{ $profiles->profile_availability}}</p>
                                </div>
                            </div>   
                            Name : {{$profiles->name}}<br>
                            Role : {{$profiles->designation}}<br>
                            Current Location: {{$profiles->current_location}} <br>
                            Phone Number: {{$profiles->phone_number}}

                        @endif
                    </div>
                <!-- ##################new div 12###################### -->
                
                <div class="col-12">
                        <form class="form-horizontal" method="POST" action="{{ url('/addProfile') }}" enctype="multipart/form-data" >
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input id="name" type="name" class="form-control" name="name" value="{{ old('name') }}" placeholder="First Name" required>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                            </div>
                             <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                <input id="middle_name" type="middle_name" class="form-control" name="middle_name" value="{{ old('middle_name') }}" placeholder="Middle Name" >

                                    @if ($errors->has('middle_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('middle_name') }}</strong>
                                        </span>
                                    @endif
                            </div>
                             <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <input id="last_name" type="last_name" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name " >

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                             <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                <input id="phone_number" type="phone_number" class="form-control" name="phone_number" value="{{ old('phone_number') }}" placeholder="phone_number" >

                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                             <div class="form-group{{ $errors->has('current_location') ? ' has-error' : '' }}">
                                    <input id="current_location" type="current_location" class="form-control" name="current_location" value="{{ old('current_location') }}" placeholder="Add current_location " >

                                    @if ($errors->has('current_location'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('current_location') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="form-group{{ $errors->has('profile_status') ? ' has-error' : '' }}">
                                    <input id="profile_status" type="profile_status" class="form-control" name="profile_status" value="{{ old('profile_status') }}" placeholder="Add profile_status " >

                                    @if ($errors->has('profile_status'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('profile_status') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="form-group{{ $errors->has('profile_availability') ? ' has-error' : '' }}">
                                    <input id="profile_availability" type="profile_availability" class="form-control" name="profile_availability" value="{{ old('profile_availability') }}" placeholder="Add profile_availability" >

                                    @if ($errors->has('profile_availability'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('profile_availability') }}</strong>
                                        </span>
                                    @endif
                            </div>
                            <div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
                                <input id="designation" type="designation" class="form-control" name="designation" value="{{ old('designation') }}" placeholder="Add designation " required>

                                @if ($errors->has('designation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('designation') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                            <div class="form-group{{ $errors->has('profile_pic') ? ' has-error' : '' }}">
                                <input id="profile_pic" type="file" class="form-control" name="profile_pic" value="{{ old('profile_pic') }}" placeholder="Add current_location " required>

                                @if ($errors->has('profile_pic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profile_pic') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Add Profile
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
               </div> <!--end of the row -->

            </div>
        </div>
    </div>

</div>

@endsection 

