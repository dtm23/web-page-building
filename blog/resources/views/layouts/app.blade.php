<?php
    error_reporting(0);
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="{{ asset('css/mainStyle.css') }}" rel="stylesheet">

</head>
<body>
<nav class="navbar fixed-top  navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <!-- Branding Image -->
        <a class="navbar-brand" href="{{ url('/home') }}">
            {{ config('app.name', 'Laravel') }}
        </a>

        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="{{ url('/post') }}">Posts <span class="sr-only">(current)</span></a>
          </li>
            <li class="nav-item active">
            <a class="nav-link" href="{{ url('/profile') }}">Profile <span class="sr-only">(current)</span></a>
          </li>
            <li class="nav-item active">
            <a class="nav-link" href="{{ url('/category') }}">Category<span class="sr-only">(current)</span></a>
          </li>
    
        </ul>
        
        <form class="form-inline my-2 my-lg-0">
             <!-- dropdown Menu below -->
            <div class="collapse navbar-collapse " style="margin: 0 auto,padding:0;float: right;" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->

                <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                                <!-- Authentication Links -->
                                @if (Auth::guest())
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>

                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{url('/profile')}}">Profile</a></li>
                                            <li><a href="{{url('/post')}}">Posts</a></li>
                                            <li><a href="{{url('/category')}}">Categories</a></li>
                                            <hr>

                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                    </ul>


            </div>       
        </form>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation"></button>
    </div> 
</nav>



<div class="container">
        <!-- Columns are always 50% wide, on mobile and desktop -->
        <div class="row">
            <div class="col-2 side-navigator" >
                <div class="card">
                    <ul>
                        <li>
                            <a href="{{url('/profile')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-home" style=""></i> Profile</a>
                        </li>
                        <li>
                            <a href="{{url('/message')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-envelope"></i> Message</a>
                        </li>
                        <li>
                            <a href="{{url('/friend')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-users"></i> Friends</a>
                        </li>
                        <li>
                            <a href="{{url('/news')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-globe"></i> News Feed</a>
                        </li>
                        <li>
                            <a href="{{url('/community')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-bookmark"></i> Communities</a>
                        </li>
                        <li>
                            <a href="{{url('/event')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-calendar"></i> Events</a></li>
                        <li>
                            <a href="{{url('/photo')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-camera-retro"></i> Photo</a>
                        </li>
                        <li>
                            <a href="{{url('/video')}}" style="font-size: 1.1em; text-decoration: none;"><i class="fa fa-video-camera"> Videos</i></a>
                        </li>
                    </ul>
                </div>
                @yield('sitenavigator')
            </div>
            <div class="col-7"">
                @yield('content')
            </div>
            <div class="col-3">
               
            </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
