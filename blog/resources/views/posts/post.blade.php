@extends('layouts.app')
@section('title')
    Posts
@endsection
@section('content')
<!-- This layout for, POSTS-CATEGORY-EDIT PROFILE, MESSAGE, COMMUNIIES, PHOTO VIDEO -->
<div class="row">
    <div class="col-12 ">
    	@if(count($errors)>0)
          @foreach($errors->all() as $error)
            <div class="alert alert-danger">$error</div>
          @endforeach
        @endif
        @if(session('message'))
          <div class="alert alert-success">{{session('message')}}</div>
        @endif
    	<div class="panel-heading text-center">ADD &nbsp; POSTS</div>
        <hr>
    	<div class="panel-body">
	        <form class="form-horizontal" method="POST" action="{{ url('/addPost') }}" enctype="multipart/form-data" >
	                {{ csrf_field() }}

	                 <div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">

	                    <div class="col-md-12">
	                        <input id="post_title" type="post_title" class="form-control" name="post_title" value="{{ old('post_title') }}" placeholder="Post Title" >

	                        @if ($errors->has('post_title'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('post_title') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>
	                 <div class="form-group{{ $errors->has('post_body') ? ' has-error' : '' }}">

	                    <div class="col-md-12">
	                        <textarea id="post_body" type="post_body" rows="5" class="form-control" name="post_body" value="{{ old('post_body') }}" placeholder="What's on your mind" required autofocus >
	                        </textarea> 

	                        @if ($errors->has('post_body'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('post_body') }}</strong>
	                            </span>
	                        @endif
	                        	
	                        
	                    </div>
	                </div>
	                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">

	                    <div class="col-md-12">
	                        <select id="category_id" type="category_id" class="form-control" name="category_id" value="{{ old('category_id') }}" placeholder="category">
	                        	@if(count($categories)>0)
	                        		@foreach($categories as $category)
	                        			<option value="{{ $category->id }} ">{{$category->category }}</option>
	                        		@endforeach
	                        	@endif
	                        </select> 

	                        @if ($errors->has('category_id'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('category_id') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                </div>
	                <div class="form-group{{ $errors->has('post_image') ? ' has-error' : '' }}">

                                <div class="col-md-12">
                                    <input id="post_image" type="file" class="form-control" name="post_image" value="{{ old('post_image') }}" placeholder="post_image" >

                                    @if ($errors->has('post_image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('post_image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
	                <div class="form-group">
	                    <div class="col-md-12 col-md-offset-2">
	                        <button type="submit" class="btn btn-primary">
	                            Add Posts
	                        </button>
	                    </div>
	                </div>
	            </form>
	        </div>       
   	</div>
</div>
@endsection