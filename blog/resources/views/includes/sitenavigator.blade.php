@extends('layouts.app')

@section('sitenavigator')
<div class="card">
                    <ul>
                        <li>
                            <a href="{{url('/profile')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-home" style=""></i> Profile</a>
                        </li>
                        <li>
                            <a href="{{url('/message')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-envelope"></i> Message</a>
                        </li>
                        <li>
                            <a href="{{url('/friend')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-users"></i> Friends</a>
                        </li>
                        <li>
                            <a href="{{url('/news')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-globe"></i> News Feed</a>
                        </li>
                        <li>
                            <a href="{{url('/community')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-bookmark"></i> Communities</a>
                        </li>
                        <li>
                            <a href="{{url('/event')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-calendar"></i> Events</a></li>
                        <li>
                            <a href="{{url('/photo')}}" style="font-size: 1.1em;text-decoration: none;"><i class="fa fa-camera-retro"></i> Photo</a>
                        </li>
                        <li>
                            <a href="{{url('/video')}}" style="font-size: 1.1em; text-decoration: none;"><i class="fa fa-video-camera"> Videos</i></a>
                        </li>
                    </ul>
                </div>
@endsection