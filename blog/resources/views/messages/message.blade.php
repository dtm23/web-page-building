@extends('layouts.app')
@section('title')
    Message
@endsection
@section('content')
<!-- This layout for, POSTS-CATEGORY-EDIT PROFILE, MESSAGE, COMMUNIIES, PHOTO VIDEO -->
<div class="row">
    <div class="col-12 ">
        <!-- Categories Main events goes here -->
    	<div class="panel panel-default">
    		@if(count($errors)>0)
    			@foreach($errors->all() as $error)
    				<div class="alert alert-danger">$error</div>
    			@endforeach
    		@endif
    		@if(session('message'))
    			<div class="alert alert-success">{{session('message')}}</div>
    		@endif
            <div class="panel-heading text-center">M E S S A G E</div>
            <hr>

            <div class="panel-body">
               


            </div>
        </div>
    </div>

</div>

@endsection 